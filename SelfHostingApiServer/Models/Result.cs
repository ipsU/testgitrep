﻿using System.Linq;
using System.Net.Http;
using HtmlAgilityPack;
using SelfHostingApiServer.Infrastructure;

namespace SelfHostingApiServer.Models
{
    public class Result
    {
        public string[] Solutions { get; set; }

        public static Result FromHttpResponse(HttpResponseMessage message)
        {
            var body = message.Content.ReadAsStringAsync().Result;
            var result = new Result();
            var tree = new HtmlDocument();

            tree.LoadHtml(body);
            var solution = tree.DocumentNode.Descendants().FirstOrDefault(d => d.Attributes["title"] != null && d.Attributes["title"].Value.Contains("olution"));
            if (solution != null)
            {
                var solutions = (from childNode in solution.ChildNodes 
                                 where childNode.Name == "subpod" 
                                 select childNode.ChildNodes[1].InnerText.RemoveNewlinesAndSpaces()).ToArray();
                result.Solutions = solutions;
            }
            else
            {
                solution = tree.DocumentNode.Descendants().First(d => d.Attributes["title"] != null && d.Attributes["title"].Value == "Result");
                result.Solutions = new[] {solution.ChildNodes[1].InnerText.RemoveNewlinesAndSpaces()};
            }

            return result;
        }
    }
}