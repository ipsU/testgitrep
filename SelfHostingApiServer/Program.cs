﻿using System;
using System.Threading;
using SelfHostingApiServer.Infrastructure;

namespace SelfHostingApiServer
{
    public class Program
    {

        public static void Main(string[] args)
        {
            var server = new CustomServer();

            Console.WriteLine("Starting api server...");

            var cts = new CancellationTokenSource();

            server.Start(cts.Token);

            Console.WriteLine("Server started");

            var s = Console.ReadLine();
            while (s != "stop")
            {
                Console.WriteLine("Write \"stop\" to stop server");
                s = Console.ReadLine();
            }

            cts.Cancel(); //прерываем слушание порта
        }
    }
}
