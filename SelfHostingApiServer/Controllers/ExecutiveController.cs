﻿using System.Web.Http;
using SelfHostingApiServer.Infrastructure;
using SelfHostingApiServer.Models;

namespace SelfHostingApiServer.Controllers
{
    public class ExecutiveController : ApiController
    {
        private readonly CalculationProvider _provider;

        public ExecutiveController(CalculationProvider provider)
        {
            _provider = provider;
        }

        [HttpGet]
        [Route("api/test")]
        public Result[] Test()
        {
            var testRequest = "x^2=9";

            var requests = new[] {new Request {Body = testRequest}};
                
            return _provider.Calculate(requests).Result;
        }

        [HttpPost]
        [Route("api/calculate")]
        public Result[] Calculate([FromBody]Request[] requests)
        {
            return _provider.Calculate(requests).Result;
        }
    }
}