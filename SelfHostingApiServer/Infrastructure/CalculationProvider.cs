﻿using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using SelfHostingApiServer.Models;

namespace SelfHostingApiServer.Infrastructure
{
    public class CalculationProvider
    {
        private const string Uri =
            "http://api.wolframalpha.com/v2/query?appid=542A2R-TH4Q8ALLG7&input={0}&format=plaintext";
        private readonly HttpClient _client;

        public CalculationProvider()
        {
            _client = new HttpClient();
        }


        public async Task<Result[]> Calculate(Request[] requests)
        {
            var tasks = requests.Select(r =>
            {
                var url = string.Format(Uri, HttpUtility.UrlEncode(r.Body));
                var req = new HttpRequestMessage(HttpMethod.Get, url);
                return _client.SendAsync(req);
            }).ToArray();

            var results = await Task.WhenAll(tasks);

            return results.Select(Result.FromHttpResponse).ToArray();
        }
    }
}