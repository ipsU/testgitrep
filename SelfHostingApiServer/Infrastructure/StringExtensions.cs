﻿using System.Text.RegularExpressions;

namespace SelfHostingApiServer.Infrastructure
{
    public static class StringExtensions
    {
        public static string RemoveNewlinesAndSpaces(this string s)
        {
            var result = s.Replace("\n", "");
            result = result.Replace(" ", "");
            result = result.Replace("", "=");
            return result;
        }
    }
}