﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace SelfHostingApiServer.Infrastructure
{
    public class CustomServer
    {
        private readonly HttpListener _listener;
        private readonly RequestProcessor _processor;

        public CustomServer()
        {
            _listener = new HttpListener();
            _listener.Prefixes.Add("http://*:88/");
            _processor = new RequestProcessor();
        }

        public async void Start(CancellationToken token)
        {
            _listener.Start();
            while (true)
            {
                var context =  await _listener.GetContextAsync(); //асинхронно получаем запрос
                Task.Factory.StartNew(() => { _processor.ProcessRequest(context); }); //затем запускаем его в обработку и ждем следующий
                token.ThrowIfCancellationRequested(); //выбрасываем исключение, если токен был отменен
            }
        }

        public void Stop()
        {
            _listener.Stop();
        }

    }
}