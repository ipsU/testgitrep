﻿using System;
using Microsoft.Owin.Hosting;

namespace SelfHostingApiServer.Infrastructure
{
    public class Server
    {
        public const string Url = "http://localhost:88";

        public void Start()
        {
            WebApp.Start<Startup>(Url);
            Console.WriteLine("Server started!");
        }

        public void Stop()
        {
            Console.WriteLine("Server is stopping...");
        }
    }
}