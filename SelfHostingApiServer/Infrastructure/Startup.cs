﻿using System.Net.Http.Headers;
using System.Web.Http;
using Castle.Windsor;
using Owin;

namespace SelfHostingApiServer.Infrastructure
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            var container = new WindsorContainer();
            container.Install(new ServerInstaller());

            var resolver = new WindsorDependencyResolver(container);
            config.DependencyResolver = resolver;
            config.MapHttpAttributeRoutes();
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            app.UseWebApi(config);
        }
    }
}