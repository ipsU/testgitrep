﻿using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using SelfHostingApiServer.Models;

namespace SelfHostingApiServer.Infrastructure
{
    public class RequestProcessor
    {
        private readonly CalculationProvider _provider;

        public RequestProcessor()
        {
            _provider = new CalculationProvider();
        }
        public void ProcessRequest(HttpListenerContext ctx)
        {
            var content = new StreamReader(ctx.Request.InputStream).ReadToEnd();
            var requests = JsonConvert.DeserializeObject<Request[]>(content);
            Thread.Sleep(10000);
            ctx.Response.StatusCode = 200;
            ctx.Response.KeepAlive = false;
            var result = _provider.Calculate(requests);
            var json = JsonConvert.SerializeObject(result);
            var b = Encoding.UTF8.GetBytes(json);
            ctx.Response.ContentLength64 = b.Length;

            var output = ctx.Response.OutputStream;
            output.Write(b, 0, b.Length);
            ctx.Response.Close();
            
        }
    }
}